package com.startup.hb.project0;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
    public void toast1(View view){
        Context context = getApplicationContext();
        // CharSequence  extra=(Button)view).getText();
        CharSequence text = "This button will launch SPOTIFY STREAMER" ;
        int duration = Toast.LENGTH_SHORT;

        Toast toast = Toast.makeText(context, text, duration);
        toast.show();
    }
    public void toast2(View view){
        Context context = getApplicationContext();
        // CharSequence  extra=(Button)view).getText();
        CharSequence text = "This button will launch SCORES APP" ;
        int duration = Toast.LENGTH_SHORT;

        Toast toast = Toast.makeText(context, text, duration);
        toast.show();
    }
    public void toast3(View view){
        Context context = getApplicationContext();
        // CharSequence  extra=(Button)view).getText();
        CharSequence text = "This button will launch LIBRARY APP" ;
        int duration = Toast.LENGTH_SHORT;

        Toast toast = Toast.makeText(context, text, duration);
        toast.show();
    } public void toast4(View view){
        Context context = getApplicationContext();
        // CharSequence  extra=(Button)view).getText();
        CharSequence text = "This button will launch BUILD IT BIGGER" ;
        int duration = Toast.LENGTH_SHORT;

        Toast toast = Toast.makeText(context, text, duration);
        toast.show();
    } public void toast5(View view){
        Context context = getApplicationContext();
        // CharSequence  extra=(Button)view).getText();
        CharSequence text = "This button will launch XYZ READER" ;
        int duration = Toast.LENGTH_SHORT;

        Toast toast = Toast.makeText(context, text, duration);
        toast.show();
    } public void toast6(View view){
        Context context = getApplicationContext();
        // CharSequence  extra=(Button)view).getText();
        CharSequence text = "This button will launch MY OWN APP" ;
        int duration = Toast.LENGTH_SHORT;

        Toast toast = Toast.makeText(context, text, duration);
        toast.show();
    }
}
